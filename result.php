<?php
require_once("SHA256.php");
require_once("MD5.php");
?>
<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>The HTML5</title>
    <meta name="description" content="The HTML5">
    <meta name="author" content="SitePoint">
</head>

<body>
<div class="wrapper" style="width: 500px; margin: 0 auto; text-align: center">
    <p>
        <?php
        // DB Data
        $servername = "localhost";
        $dbusername = "root";
        $dbpassword = "";
        $dbname = "mydb";

        // POST Data
        $login = $_POST["login"];
        $password = $_POST["password"];
        $encryptionType = $_POST["encryptionType"];

        // DEBUG
//        $password = "valik";
//        $encryptionType = "MD5";

        // Encrypting
        $start = microtime(true);
        // SHA256
        if ($encryptionType == "SHA256"){
            $cryptor = new SHA256();
            $password = $cryptor->make($password);
        }
        // MD5
        else if ($encryptionType == "MD5") {
            $cryptor = new MD5();
            $password = $cryptor->make($password);
        }
        $end = microtime(true);
        $runtime = ($end - $start);

        $conn = new mysqli($servername, $dbusername, $dbpassword, $dbname);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $sql = "INSERT INTO users (login, password, encryptionType, runtime) VALUES ('$login', '$password', '$encryptionType', $runtime)";

        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully: <br>Login: $login<br>Password: $password<br>Runtime: $runtime";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        $conn->close();
        ?>
    </p>
</div>
</body>
</html>